# Build image
In this example, both Dockerfiles are identical because we are simply deploying a static HTML page. However, in a real-world scenario, the Dockerfiles for blue and green deployments may differ depending on the changes being made.

To perform a blue/green deployment using these Dockerfiles, you would create two Docker images using the docker build command:

### Build Blue
```
docker build -t abdelranyhammoumi/blue-green-lab:blue -f Dockerfile .
```

### Build Green
Before running `docker build` we need to change our `index.html` file.

Comment blue div and uncomment green div:
```html
	<!-- div class="blue">
		Welcome to Blue
	</div -->
	<div class="green">
		Welcome to Green
	</div>
```

Now we build green image with `":green"` tag:
```
docker build -t abdelranyhammoumi/blue-green-lab:green -f Dockerfile .
```

# Push image to dockerhub
```
docker login -u abdelranyhammoumi
```
```
docker push abdelranyhammoumi/blue-green-lab:blue
docker push abdelranyhammoumi/blue-green-lab:green
```
# Push image to gitlab
We need to change the image `tag` and login to gitlab registry (`registry.gitlab.com`):

### Change tag
```
docker tag abdelranyhammoumi/blue-green-lab:blue registry.gitlab.com/r4n7/minikube-lab/blue-green:blue
```
```
docker tag abdelranyhammoumi/blue-green-lab:green registry.gitlab.com/r4n7/minikube-lab/blue-green:green
```

### Login to gitlab registry
```
docker login registry.gitlab.com -u r4n7
```

### Push to gitlab
```
docker push registry.gitlab.com/r4n7/minikube-lab/blue-green:blue
```
```
docker push registry.gitlab.com/r4n7/minikube-lab/blue-green:green
```

# Test image
We can test our blue and green images with `docker run`, to process our nginx web servers, with:
- `-d` for `detach` : to run container in background and print container ID
- `-p` for `publish` : to publish a container's port(s) to the host
- `8888` : will be our local machine port number
- `80` : the container port exposed by nginx

>| **8888**   | **:**      | **80**         |
>|:----------:|:----------:|:--------------:|
>| local port | forward to | container port |

```
docker run -d -p 8888:80 abdelranyhammoumi/blue-green-lab:blue
```

Then open in browser `http://localhost:8888/`

Or test with `curl`

```
curl localhost:8888
```
